package main

import (
	"fmt"

	"bitbucket.org/_metalogic_/diff"
	"bitbucket.org/_metalogic_/log"
)

func main() {
	d := diff.UnifiedDiff{
		A:        []string{"now is the time for all good men\n to come to the aid of the party\n"},
		B:        []string{"now is the time for all good women\n to come to the aid of the party\n"},
		FromFile: "Original",
		ToFile:   "Current",
		Context:  3,
	}
	text, err := diff.GetUnifiedDiffString(d)
	if err != nil {
		log.Error(err)
	}
	fmt.Printf(text)
}
